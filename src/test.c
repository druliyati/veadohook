#include "veadohook.h"
#include <stdio.h>

#define KEYNAME_BUFFER_MAX 1024
vchar_t keyname_buffer[KEYNAME_BUFFER_MAX];

void callback(int32_t hook, void* data) {
	switch (hook) {
		case HOOK_MOUSE:
		{
			hook_mouse_t* mouse = (hook_mouse_t*)data;
			printf("[mouse] %d: %d\n", mouse->event_type, mouse->xbutton);
			break;
		}
		case HOOK_KEYBOARD:
		{
			hook_keyboard_t* keyboard = (hook_keyboard_t*)data;
			uint32_t len = veadohook_key_name(keyboard->key, keyname_buffer, KEYNAME_BUFFER_MAX);
			printf("[keyboard] %d: [%d] %d (%d: \"", keyboard->event_type, veadohook_key_source(), keyboard->key, len);
			for (uint32_t a = 0; a < len; a++) {
				printf("%lc", keyname_buffer[a]);
			}
			printf("\")\n");
			break;
		}
		case HOOK_CURSOR:
		{
			hook_cursor_t* cursor = (hook_cursor_t*)data;
			printf("[cursor] (%d, %d) (%d, %d)\n", cursor->x, cursor->y, cursor->width, cursor->height);
			break;
		}
		default: printf("[unknown event]\n"); break;
	}
}

void print_hook(int32_t hook, const char* s) {
	printf("%d: %s (available: %d)\n", hook, s, veadohook_hook_available(hook));
}

int main() {
	printf("[veadohook]\n");
	veadohook_init();
	printf("\n");
	while (1) {
		printf("select hook:\n");
		print_hook(HOOK_MOUSE, "mouse");
		print_hook(HOOK_KEYBOARD, "keyboard");
		print_hook(HOOK_CURSOR, "cursor");
		printf("> ");
		int32_t hook;
#ifdef PLATFORM_WIN
		scanf_s("%d", &hook);
#else
		scanf("%d", &hook);
#endif
		if (veadohook_listen(hook, callback)) {
			printf("hooked to %d. press enter to stop.\n", hook);
			while (getchar() != '\n');
			while (getchar() != '\n');
			if (veadohook_unlisten(hook, callback)) {
				printf("unhooked successfully.\n");
			} else {
				printf("couldn't unhook for some reason?\n");
			}
		} else {
			printf("couldn't hook to %d\n", hook);
		}
		printf("\n");
	}
	exit(0);
}
