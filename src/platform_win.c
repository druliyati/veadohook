#include "veadohook.h"
#ifdef PLATFORM_WIN

#include <stdio.h>

#define THREAD_MOUSE_BEGIN (WM_APP + 1)
#define THREAD_MOUSE_END (WM_APP + 2)
#define THREAD_KEYBOARD_BEGIN (WM_APP + 3)
#define THREAD_KEYBOARD_END (WM_APP + 4)

uint32_t enable_count = 0;
uint32_t mouse_count = 0;
uint32_t keyboard_count = 0;

HHOOK mouse_hook = NULL;
HHOOK keyboard_hook = NULL;

HINSTANCE dll_instance;
HANDLE thread;
HWND window;
uint32_t thread_ready;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	dll_instance = (HINSTANCE)hModule;
	return TRUE;
}

int32_t mouse_proc_to_struct(WPARAM wParam, LPARAM lParam, hook_mouse_t* mouse, hook_cursor_t* cursor) {
	MSLLHOOKSTRUCT* msll = (MSLLHOOKSTRUCT*)lParam;
	switch (wParam) {

	case WM_MOUSEMOVE:
		RECT rect;
		if (GetWindowRect(GetDesktopWindow(), &rect)) {
			LONG width = rect.right - rect.left;
			LONG height = rect.bottom - rect.top;
			if (width > 1 && height > 1) {
				cursor->x = min(max(0, msll->pt.x), width - 1);
				cursor->y = min(max(0, msll->pt.y), height - 1);
				cursor->width = width;
				cursor->height = height;
			} else {
				cursor->x = max(0, msll->pt.x);
				cursor->y = max(0, msll->pt.y);
				cursor->width = 0;
				cursor->height = 0;
			}
		} else {
			cursor->x = 0;
			cursor->y = 0;
			cursor->width = 0;
			cursor->height = 0;
		}
		return 2;

	case WM_LBUTTONDOWN: mouse->event_type = HOOK_MOUSE_EVENT_LBUTTON_DOWN; return 1;
	case WM_LBUTTONUP: mouse->event_type = HOOK_MOUSE_EVENT_LBUTTON_UP; return 1;
	case WM_RBUTTONDOWN: mouse->event_type = HOOK_MOUSE_EVENT_RBUTTON_DOWN; return 1;
	case WM_RBUTTONUP: mouse->event_type = HOOK_MOUSE_EVENT_RBUTTON_UP; return 1;
	case WM_MBUTTONDOWN: mouse->event_type = HOOK_MOUSE_EVENT_MBUTTON_DOWN; return 1;
	case WM_MBUTTONUP: mouse->event_type = HOOK_MOUSE_EVENT_MBUTTON_UP; return 1;

	case WM_XBUTTONDOWN:
		mouse->event_type = HOOK_MOUSE_EVENT_XBUTTON_DOWN;
		mouse->xbutton = msll->mouseData >> 16;
		return 1;

	case WM_XBUTTONUP:
		mouse->event_type = HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse->xbutton = msll->mouseData >> 16;
		return 1;

	default: return 0;
	}
}

int32_t keyboard_proc_to_struct(WPARAM wParam, LPARAM lParam, hook_keyboard_t* keyboard) {
	KBDLLHOOKSTRUCT* kbdll = (KBDLLHOOKSTRUCT*)lParam;
	keyboard->event_type = (kbdll->flags & LLKHF_UP) ? HOOK_KEYBOARD_EVENT_KEY_UP : HOOK_KEYBOARD_EVENT_KEY_DOWN;
	keyboard->key = kbdll->vkCode;
	return 1;
}

static hook_cursor_t cursor_prev = {0};
LRESULT CALLBACK mouse_proc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode >= 0) {
		hook_mouse_t mouse;
		hook_cursor_t cursor;
		switch (mouse_proc_to_struct(wParam, lParam, &mouse, &cursor)) {
			case 1: veadohook_emit(HOOK_MOUSE, &mouse); break;
			case 2:
				if (cursor_prev.x != cursor.x || cursor_prev.y != cursor.y || cursor_prev.width != cursor.width || cursor_prev.height != cursor.height) {
					cursor_prev = cursor;
					veadohook_emit(HOOK_CURSOR, &cursor);
				}
				break;
		}
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}

LRESULT CALLBACK keyboard_proc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode >= 0) {
		hook_keyboard_t keyboard;
		if (keyboard_proc_to_struct(wParam, lParam, &keyboard)) {
			veadohook_emit(HOOK_KEYBOARD, &keyboard);
		}
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}

DWORD WINAPI thread_proc(void* data) {
	printf("[veadohook] hi from the win thread!\n");
	Sleep(50);
	window = CreateWindowExW(0, L"Static", L"", 0, 0, 0, 100, 100, NULL, NULL, dll_instance, NULL);
	UpdateWindow(window);
	thread_ready = 1;
	MSG msg;
	while (1) {
		int result = GetMessage(&msg, NULL, 0, 0);
		if (result == -1 || result == 0 || msg.message == WM_QUIT) break;
		TranslateMessage(&msg);
		switch (msg.message) {

		case THREAD_MOUSE_BEGIN:
			if (mouse_hook == NULL) {
				printf("[veadohook] begin mouse hook\n");
				mouse_hook = SetWindowsHookEx(14, mouse_proc, dll_instance, 0);
			}
			break;

		case THREAD_MOUSE_END:
			if (mouse_hook != NULL) {
				printf("[veadohook] end mouse hook\n");
				UnhookWindowsHookEx(mouse_hook);
				mouse_hook = NULL;
			}
			break;

		case THREAD_KEYBOARD_BEGIN:
			if (keyboard_hook == NULL) {
				printf("[veadohook] begin keyboard hook\n");
				keyboard_hook = SetWindowsHookEx(13, keyboard_proc, dll_instance, 0);
			}
			break;

		case THREAD_KEYBOARD_END:
			if (keyboard_hook != NULL) {
				printf("[veadohook] end keyboard hook\n");
				UnhookWindowsHookEx(keyboard_hook);
				keyboard_hook = NULL;
			}
			break;

		}
		DispatchMessage(&msg);
	}
	DestroyWindow(window);
	printf("[veadohook] bye from the win thread!\n");
	return 0;
}

void thread_send(UINT msg) {
	if (thread != NULL) PostThreadMessageW(GetThreadId(thread), msg, NULL, NULL);
}

int32_t hook_g(int32_t enable, uint32_t* count, UINT msgBegin, UINT msgEnd) {
	if (enable) {
		enable_count++;
		if (thread == NULL) {
			thread_ready = 0;
			thread = CreateThread(NULL, 0, thread_proc, NULL, 0, NULL);
		}
		while (!thread_ready) Sleep(1);
		if (!(*count)) thread_send(msgBegin);
		(*count)++;
	} else {
		if (!(*count)) return 0;
		if (enable_count > 0) enable_count--;
		(*count)--;
		if (!(*count)) thread_send(msgEnd);
		if (thread != NULL && enable_count == 0) {
			thread_send(WM_QUIT);
			thread = NULL;
		}
	}
	return 1;
}

int32_t mouse(int32_t enable) {
	return hook_g(enable, &mouse_count, THREAD_MOUSE_BEGIN, THREAD_MOUSE_END);
}

int32_t keyboard(int32_t enable) {
	return hook_g(enable, &keyboard_count, THREAD_KEYBOARD_BEGIN, THREAD_KEYBOARD_END);
}

uint32_t keyname(uint32_t key, vchar_t* str, uint32_t capacity) {
	uint32_t scan = MapVirtualKeyEx(key, MAPVK_VK_TO_VSC_EX, GetKeyboardLayout(0));
	switch (key) {
	case VK_LEFT: case VK_UP: case VK_RIGHT: case VK_DOWN:
	case VK_RCONTROL: case VK_RMENU:
	case VK_LWIN: case VK_RWIN: case VK_APPS:
	case VK_PRIOR: case VK_NEXT:
	case VK_END: case VK_HOME:
	case VK_INSERT: case VK_DELETE:
	case VK_DIVIDE:
	case VK_NUMLOCK:
		scan |= KF_EXTENDED;
		break;
	}
	wchar_t str_wchar[128];
	int len_wchar = GetKeyNameTextW(scan << 16, str_wchar, capacity < 128 ? capacity : 128);
	uint32_t len;
	for (len = 0; len < 128 && len < capacity && len < len_wchar && str_wchar[len] != '\0'; len++) {
		str[len] = str_wchar[len];
	}
	return len;
}

void veadohook_platform_setup() {
	hookapi_proc[HOOK_MOUSE] = mouse;
	hookapi_proc[HOOK_KEYBOARD] = keyboard;
	hookapi_proc[HOOK_CURSOR] = mouse;
	hookapi_keysource = HOOK_KEYBOARD_SOURCE_WINDOWS_HOOK;
	hookapi_keyname_proc = keyname;
}

#endif
