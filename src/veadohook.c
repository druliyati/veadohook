#include "veadohook.h"

#define CALLBACK_MAX 64

typedef struct {
	hookcallback_t callbacks[CALLBACK_MAX];
	uint32_t callback_count;
} callbacklist_t;

callbacklist_t callback_list[HOOK_MAX];

hookapi_t hookapi_proc[HOOK_MAX];
uint8_t hookapi_keysource;
hookapikeyname_t hookapi_keyname_proc;

int emit_tl_init = 0;
EMIT_TL_DECLARE();

void emit_tl_lock() {
	if (!emit_tl_init) {
		emit_tl_init = 1;
		EMIT_TL_INIT();
	}
	EMIT_TL_LOCK();
}

void emit_tl_unlock() {
	EMIT_TL_UNLOCK();
}

int32_t init = 0;
DLLEXPORT void veadohook_init() {
	if (init) return;
	init = 1;
	for (int32_t a = 0; a < HOOK_MAX; a++) hookapi_proc[a] = NULL;
	veadohook_platform_setup();
}

hookapi_t hook_api_get(int32_t hook) {
	if (hook < 0 || hook >= HOOK_MAX) return 0;
	veadohook_init();
	return hookapi_proc[hook];
}

DLLEXPORT int32_t veadohook_hook_available(int32_t hook) {
	veadohook_init();
	return hook_api_get(hook) != NULL;
}

DLLEXPORT int32_t veadohook_listen(int32_t hook, hookcallback_t callback) {
	if (callback == NULL) return 0;
	hookapi_t api = hook_api_get(hook);
	if (api == NULL) return 0;
	callbacklist_t* c = callback_list + hook;
	emit_tl_lock();
	uint32_t count = c->callback_count;
	emit_tl_unlock();
	if (count == 0) {
		if (!api(1)) return 0;
	} else if (count >= CALLBACK_MAX) {
		return 0;
	}
	emit_tl_lock();
	c->callbacks[c->callback_count] = callback;
	c->callback_count++;
	emit_tl_unlock();
	return 1;
}

DLLEXPORT int32_t veadohook_unlisten(int32_t hook, hookcallback_t callback) {
	if (callback == NULL) return 0;
	hookapi_t api = hook_api_get(hook);
	if (api == NULL) return 0;
	callbacklist_t* c = callback_list + hook;
	int32_t ret = 0;
	int disable = 0;
	emit_tl_lock();
	if (c->callback_count > 0) {
		uint32_t a = c->callback_count - 1;
		int found = 0;
		while (1) {
			if (c->callbacks[a] == callback) {
				found = 1;
				break;
			}
			if (a == 0) break;
			a--;
		}
		if (found) {
			c->callback_count--;
			for (; a < c->callback_count; a++) {
				c->callbacks[a] = c->callbacks[a + 1];
			}
			ret = 1;
			disable = c->callback_count == 0;
		}
	}
	emit_tl_unlock();
	if (disable) api(0);
	return ret;
}

DLLEXPORT uint8_t veadohook_key_source() {
	veadohook_init();
	return hookapi_keysource;
}

DLLEXPORT uint32_t veadohook_key_name(uint32_t key, vchar_t* str, uint32_t capacity) {
	veadohook_init();
	if (hookapi_keyname_proc == NULL) return 0;
	uint32_t len = hookapi_keyname_proc(key, str, capacity);
	if (len < capacity) str[len] = '\0';
	return len;
}

#ifdef PLATFORM_UNSUPPORTED
void veadohook_platform_setup() {}
#endif

void veadohook_emit(int32_t hook, void* data) {
	if (hook < 0 || hook >= HOOK_MAX) return;
	emit_tl_lock();
	for (uint32_t a = 0; a < callback_list[hook].callback_count; a++) {
		callback_list[hook].callbacks[a](hook, data);
	}
	emit_tl_unlock();
}
