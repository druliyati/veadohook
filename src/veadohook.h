#ifndef VEADOHOOK
#define VEADOHOOK

#include <stdlib.h>
#include <stdint.h>

#if defined(_WIN32) || defined(_WIN64)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define PLATFORM_WIN
#define DLLEXPORT __declspec(dllexport)
#define PROCEXPORT __stdcall
#define EMIT_TL_DECLARE() HANDLE emit_tl_mutex
#define EMIT_TL_INIT() emit_tl_mutex = CreateMutex(NULL, FALSE, NULL)
#define EMIT_TL_LOCK() if (emit_tl_mutex != 0) WaitForSingleObject(emit_tl_mutex, INFINITE)
#define EMIT_TL_UNLOCK() if (emit_tl_mutex != 0) ReleaseMutex(emit_tl_mutex)

#elif defined(__linux__) || defined(__linux) || defined(linux)

#include <pthread.h>

#define PLATFORM_LINUX
#define DLLEXPORT __attribute__((visibility("default")))
#define PROCEXPORT
#define EMIT_TL_DECLARE() pthread_mutex_t emit_tl_mutex
#define EMIT_TL_INIT() pthread_mutex_init(&emit_tl_mutex, NULL)
#define EMIT_TL_LOCK() pthread_mutex_lock(&emit_tl_mutex)
#define EMIT_TL_UNLOCK() pthread_mutex_unlock(&emit_tl_mutex)

#elif defined(__APPLE__)

#include <pthread.h>

#define PLATFORM_MAC
#define DLLEXPORT
#define PROCEXPORT
#define EMIT_TL_DECLARE() pthread_mutex_t emit_tl_mutex
#define EMIT_TL_INIT() pthread_mutex_init(&emit_tl_mutex, NULL)
#define EMIT_TL_LOCK() pthread_mutex_lock(&emit_tl_mutex)
#define EMIT_TL_UNLOCK() pthread_mutex_unlock(&emit_tl_mutex)

#else

#define PLATFORM_UNSUPPORTED
#define DLLEXPORT
#define PROCEXPORT
#define EMIT_TL_DECLARE()
#define EMIT_TL_INIT()
#define EMIT_TL_LOCK()
#define EMIT_TL_UNLOCK()

#endif

typedef uint32_t vchar_t;

typedef void(PROCEXPORT *hookcallback_t)(int32_t, void*);
typedef int32_t(*hookapi_t)(int32_t);
typedef uint32_t(*hookapikeyname_t)(uint32_t, vchar_t*, uint32_t);

DLLEXPORT void veadohook_init();
DLLEXPORT int32_t veadohook_hook_available(int32_t hook);
DLLEXPORT int32_t veadohook_listen(int32_t hook, hookcallback_t callback);
DLLEXPORT int32_t veadohook_unlisten(int32_t hook, hookcallback_t callback);
DLLEXPORT uint8_t veadohook_key_source();
DLLEXPORT uint32_t veadohook_key_name(uint32_t key, vchar_t* str, uint32_t capacity);

void veadohook_platform_setup();
void veadohook_emit(int32_t hook, void* data);

#define HOOK_MOUSE 0
#define HOOK_KEYBOARD 1
#define HOOK_CURSOR 2
#define HOOK_MAX 3

extern hookapi_t hookapi_proc[HOOK_MAX];
extern uint8_t hookapi_keysource;
extern hookapikeyname_t hookapi_keyname_proc;

#define HOOK_MOUSE_EVENT_LBUTTON_DOWN 0
#define HOOK_MOUSE_EVENT_LBUTTON_UP 1
#define HOOK_MOUSE_EVENT_RBUTTON_DOWN 2
#define HOOK_MOUSE_EVENT_RBUTTON_UP 3
#define HOOK_MOUSE_EVENT_MBUTTON_DOWN 4
#define HOOK_MOUSE_EVENT_MBUTTON_UP 5
#define HOOK_MOUSE_EVENT_XBUTTON_DOWN 6
#define HOOK_MOUSE_EVENT_XBUTTON_UP 7

typedef struct {
	uint8_t event_type;
	uint32_t xbutton;
} hook_mouse_t;

#define HOOK_KEYBOARD_EVENT_KEY_DOWN 0
#define HOOK_KEYBOARD_EVENT_KEY_UP 1
#define HOOK_KEYBOARD_SOURCE_UNDEFINED 0
#define HOOK_KEYBOARD_SOURCE_WINDOWS_HOOK 1
#define HOOK_KEYBOARD_SOURCE_LINUX_INPUT 2
#define HOOK_KEYBOARD_SOURCE_MAC_EVENT_TAP 3

typedef struct {
	uint8_t event_type;
	uint32_t key;
} hook_keyboard_t;

typedef struct {
	uint32_t x, y, width, height;
} hook_cursor_t;

#endif
